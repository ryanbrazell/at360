#### Introduction
During the spring 2016 semester, the Center for Teaching, Learning, and Technology (CTLT) was reorganized out from under Information Services to report to the Provost. A year later, due in part to turnover in the Provost's Office, the department had received no new guidance or vision as to how this change would or should meaningfully impact our focus, scope, or operation. In the summer of 2017, the CTLT undertook a departmental strategic planning process, identifying (among others) a need to develop a broader and more comprehensive understanding of how faculty use academic technology at the University of Richmond. The authors of this report volunteered to lead the effort, which become known as AT360, short for Academic Technology 360º.

(The University of Richmond participates in the [MISO Survey](https://www.misosurvey.org/) every three years, but the data gathered via that instrument are both less specific and less frequent than would be useful for our needs. Also, the data from that research were historically not shared outside a very small group, and we aim to be transparent with our data, methods, outcomes, and the resulting decision-making.) [This is also a survey which measures satisfaction, and we are more interested in utilization.]

With this in mind, we developed AT360 for the immediate purpose of gathering baseline data from the 2017-18 academic year, and with the longer-term goal of creating an evaluation process that could be repeated on an annual or biennial basis, providing opportunities for longitudinal analysis. Our research questions for the inaugural edition of this evaluation are: what academic technologies are faculty at the University of Richmond using, and do usage rates of academic technologies among faculty at UR vary by discipline? The answers to these questions are important for determining how to allocate resources to meet existing and future needs. [Add information about IRB / distribution efforts / this is internal and not for public distribution]

#### Data and Methods
The data used in this analysis come from a survey administered by the CTLT in the spring of 2018, using a multistage sampling design. We first used convenience sampling, asking faculty with whom we already have established professional relationships to participate. In the second stage, we issued a blanket invitation to all faculty using UR's faculty listserv (see Appendix 1). The third and final stage used proportional random sampling; we defined a set of academic disciplines based on a structure used within the CTLT (see Appendix 2), identified the disciplines that appeared to be underrepresented in the initial data, and encouraged faculty from those disciplines to participate. Our final sample consists of 108 responses, all submitted electronically through Qualtrics.

In order to test the representativeness of this sample in terms of disciplinary demographics, we assigned faculty responders into disciplinary categories, grouping anonymous responses into an Unlisted category, and performed chi-square ("goodness of fit") testing using over faculty demographics we collected (see Appendix 3). Our null hypothesis is that the observed and expected frequencies for each discipline do not differ significantly; the research hypothesis is that observed and expected frequencies do differ significantly. When the Unlisted category is included in the analysis, we can reject the null hypothesis; the sample differs significantly from the overall faculty at the University of Richmond (X<sup>2</sup> = 22.72, df = 9, p<.01). However, when responses from the Unlisted category are excluded, we cannot reject the null hypothesis; there is no evidence that the sample differs significantly from the overall faculty at the University of Richmond (X<sup>2</sup> = 13.21, df = 8, p>.10).

This is not a perfect measure by any means. First, thirty respondents chose to remain anonymous, and as a result we were unable to assign them to a disciplinary category. Also, the demographics used to test representativeness excludes adjuncts and emeritus professors (see Appendix 3), but adjunct and emeritus professors may be present in the sample. Also, directory listings do not always make the teaching status of department members clear, so the counts used for the overall demographics may be inaccurate. Still, we have reason to believe that, provided we exclude respondents whose discipline could not be identified, our sample is representative of UR faculty by discipline. In future iterations of this research, we will require that respondents self-identify their discipline while allowing them to otherwise remain anonymous.

+ How reliable are these data?
+ What data sources are we pulling from?
+ What are we not capturing?

#### Variables of Interest


running analysis at the individual technology level, not the category level

collapsing spatial / non-spatial visualization into visualization

collapse existing / new data into uses data

collapse LMS

collapse CMS

collapse web publishing

all other categories will remain separate for the analysis


#### Results & Discussion

Andrew will run ANOVA tests

+ Limitation of current year's data (we can't do trend analyses / this is a baseline)
+ Interesting results
  + Most academic technology tools aren't widely used
    + discipline specific use cases
    + driven by licensing, deployment, and content specific uses
    + Emerging Technologies vs "Infrastructural Technologies"
  + Action items
    + Blackboard & Classroom projection: split between support of the technology & support of the people
  + Improvements for next year
  + Questions for future analysis, focusing on discipline level (not dept)

#### Appendix 1: AT360 Recruitment Email
In the summer of 2017, the Center for Teaching, Learning, and Technology identified a need to develop a broader and more comprehensive departmental understanding of how academic technology is used at the University of Richmond. This voluntary survey, which should take 5-10 minutes to complete, aims to collect information from faculty in all five schools about their academic technology use.

(link to survey)

The results of this survey will be reported in an aggregated manner that will not identify respondents, and will be used to improve the CTLT's existing programming and support services, as well as to identify areas where future growth may be needed. Questions about the survey and/or results can be directed to Andrew Bell (abell4@richmond.edu) or Ryan Brazell (rbrazell@richmond.edu).

#### Appendix 2: Departments Represented Within Each Discipline
The CTLT assigns a set of departments to each Academic Technology Consultant, who then serves as a liaison between the CTLT and faculty in those areas. For this analysis, each response was assigned to a discipline based on the primary department of the responding faculty member, using the same groupings assigned to each CTLT liaison. The Business, Law, Leadership, and Professional and Continuing Studies categories reflect responses from faculty within those schools. Responses from faculty within the School of Arts and Sciences are grouped into the following categories.

| Fine Arts          | Humanities        | Languages<sup>1</sup>                                  | Sciences                         | Social Sciences                    |
| :----------------- | :---------------- | :------------------------------------------ | :------------------------------- | :--------------------------------- |
| Art & Art History  | Classical Studies | Languages, Literatures, and Cultures        | Biology                          | Education (undergraduate)<sup>2</sup>        |
| Music              | English           | Latin American, Latino, and Iberian Studies | Chemistry                        | Geography and the Environment      |
| Theatre and Dance  | History           |                                             | Mathematics and Computer Science | Journalism                         |
|                    | Philosophy        |                                             | Physics                          | Military Science and Leadership    |
|                    | Religious Studies |                                             | Psychology                       | Political Science                  |
|                    |                   |                                             |                                  | Rhetoric and Communication Studies |
|                    |                   |                                             |                                  | Sociology and Anthropology         |

<sup>1</sup> LLC and LALIS have received their own category here because the CTLT does not have a liaison designated for the languages; for many years that role has been filled by the Director of the Global Studio.

<sup>2</sup> Faculty in the Education department who teach as part of the post-graduate and certificate programs are classified under SPCS.

#### Appendix 3: Disciplinary Demographics of Faculty (Overall and Sample)
In order to determine whether the sample is representative of UR faculty by discipline, we collected information about the number of faculty in each discipline. To gather this information, we visited the websites of each school and counted the number of tenured, tenure-track, and visiting faculty in each department. Adjunct and emeritus faculty, as well as non-teaching members (e.g. lab managers), were excluded from these counts.

| Discipline      | N, UR Faculty | %, UR Faculty | N, Sample | %, Sample<sup>1</sup> | %, Sample (excluding unlisted) |
| :-------------- | :------------ | :------------ | :-------- | :-------- | :----------------------------- |
| Business        | 70            | 16            | 8         | 7         | 10                             |
| Fine Arts       | 29            | 7             | 3         | 3         | 4                              |
| Humanities      | 58            | 14            | 10        | 9         | 13                             |
| Languages       | 33            | 8             | 7         | 6         | 9                              |
| Law             | 48            | 11            | 5         | 5         | 6                              |
| Leadership      | 16            | 4             | 7         | 6         | 9                              |
| Sciences        | 91            | 21            | 21        | 19        | 27                             |
| Social Sciences | 66            | 15            | 11        | 10        | 14                             |
| SPCS            | 16            | 4             | 6         | 6         | 8                              |
| Unlisted        | n/a           | n/a           | 30        | 28        | n/a                            |
| TOTAL           | 427           | 100           | 108       | 99        | 100                            |

<sup>1</sup> Percentages do not add up to 100 due to rounding.

#### Appendix 4: AT360 Questionnaire
This appendix contains a complete listing of questions included on the AT360 survey. All questions (including identifying information) were optional. Unless marked otherwise, all questions were multi-select items. Free-text entry questions and responses are marked with a box ( <sup>[]</sup> ), and single-select questions and responses are marked with an asterisk ( * ).


##### Section 1: Introduction and Personal Information

In the summer of 2017, the Center for Teaching, Learning, and Technology identified a need to develop a broader departmental understanding of how academic technology is used at the University of Richmond. This survey, which should take about 5-10 minutes to complete, aims to collect information from faculty in all five schools about their technology use. This data will be used to improve our existing programming and support services, as well as to identify areas where future growth may be needed.

Participation in this survey is completely voluntary. You may answer as few or as many questions as you wish. Please be assured that your responses will be strictly confidential. Any findings based on this survey will be reported in an aggregated manner that will not identify respondents.

As you complete this survey, consider what tools you have used in the last three semesters.

If you have any questions or would like to provide further feedback, please contact Andrew Bell (abell4@richmond.edu) or Ryan Brazell (rbrazell@richmond.edu).

+ Question 1: Your name <sup>[]</sup>


+ Question 2: Your email address <sup>[]</sup>


##### Section 2: Learning / Content Management and Web Publishing

+ Question 3: Which tools within Blackboard for you use? Select all that apply.
  + I do not use Blackboard.
  + Content management
  + Student photos
  + Assessments and/or assignments
  + Grade Center
  + Other (please specify): <sup>[]</sup>


+ Question 4: Do you use another system for managing course materials? Select all that apply.
  + Box
  + Dropbox
  + Google Drive
  + None
  + Other (please specify): <sup>[]</sup>


+ Question 5A: Do your students engage in web publishing for course assignments? Select all that apply.
  + Blogging (WordPress, Tumblr, Medium)
  + Social media (Twitter, Facebook, Instagram)
  + Digital exhibitions (Omeka, etc)
  + None
  + Other (please specify): <sup>[]</sup>


+ Question 5B: Do you use UR Blogs (http://blog.richmond.edu) or another WordPress-based website in your teaching?<sup>1</sup>*
  + Yes
  + No


+ Question 5C: How would you describe your level of comfort using these platforms?<sup>2</sup> <sup>[]</sup>


<sup>1</sup> This question was only presented to respondents who, on question 5A, said their students engaged in blogging.

<sup>2</sup> This question was only presented to respondents who, on question 5A, said their students engaged in blogging, social media, digital exhibitions, and/or another type of web publishing they specified.

##### Section 3: Classroom Technology


+ Question 6A: Which of the following classroom technologies do you use? Select all that apply.
  + Classroom projection system
  + Interactive whiteboards
  + Classroom polling (clickers, web-based polling)
  + Skype / video conferencing to interact with a remote guest or audience
  + Lecture capture to record in-class lectures or presentations (e.g. Panopto)
  + Desktop capture to create supplementary content or videos (e.g. Panopto, Camtasia)
  + None
  + Other (please specify): <sup>[]</sup>


+ Question 6B: How frequently do you use built-in projection systems?<sup>3</sup>*
  + Sometimes
  + About half the time
  + Most of the time
  + Always


+ Question 6C: Which polling technologies do you use? Select all that apply.<sup>4</sup>
  + Turning Point clickers
  + Poll Everywhere
  + Other (please specify): <sup>[]</sup>


+ Question 6D: Which best describes your use of desktop capture?<sup>5</sup>*
  + I occasionally create a few short videos to supplement in-class lectures.
  + I create several videos as part of a blended or flipped course model.
  + My course is fully online.

<sup>3</sup> This question was only presented to respondents who, on question 6A, said they used classroom projection systems.

<sup>4</sup> This question was only presented to respondents who, on question 6A, said they used classroom polling.

<sup>5</sup> This question was only presented to respondents who, on question 6A, said they used desktop capture.

##### Section 4: Data Education

+ Question 7A: Do your students engage in the following data-related activities as part of your courses? Select all that apply.
  + Working with existing data
  + Generation / collection of new data
  + Spatial data visualization
  + Non-spatial data visualization
  + Spatial analysis
  + Statistical analysis
  + None
  + Other (please specify): <sup>[]</sup>


+ Question 7B: Which of the following best describe the spatial analysis projects in your courses?<sup>6</sup>
  + Individual or group projects or assignments that are generally started and finished within one or two weeks.
  + Projects designed and completed over the course of several weeks or a semester. This may include a moderate amount of assistance from support departments on campus (the CTLT, Spatial Analysis Lab, etc).
  + Projects designed and completed over the course of multiple semesters. This may include a large amount of assistance from support departments on campus, particularly the Spatial Analysis Lab.


<sup>6</sup> This question was only presented to respondents who, on question 7A, said their students engaged in spatial analysis as part of their courses.

##### Section 5: Digital Media

+ Question 8A: Do students in your courses create digital media projects or assignments? Select all that apply.
  + Video projects or assignments
  + Audio projects or assignments
  + Photography or graphic design projects or assignments
  + None
  + Other (please specify): <sup>[]</sup>


+ Question 8B: Which best describes your video projects or assignments? Select all that apply. <sup>7</sup>
  + Digital story
  + Documentary
  + Digital lab report
  + Narrative scene
  + Whiteboard animation
  + Other (please specify): <sup>[]</sup>


+ Question 8C: How do your students submit their digital media assignments? Select all that apply. <sup>8</sup>
  + Upload video to a non-UR website (YouTube, Vimeo, etc).
  + Upload audio to a non-UR website (Soundcloud, etc).
  + Upload images to a non-UR website (Flickr, Imgur, etc).
  + File upload to Box
  + File upload to Blackboard
  + Other (please specify): <sup>[]</sup>


<sup>7</sup> This question was only presented to respondents who, on question 8A, said their students created video projects or assignments.

<sup>8</sup> This question was only presented to respondents who, on question 8A, said their students created video projects or assignments, audio projects or assignments, and/or photography or graphic design projects or assignments.


##### Section 6: Emerging Technologies / Poster Printing

+ Question 9A: Do your students use emerging technologies for coursework or class interactions? Select all that apply.
  + 3D printing
  + 3D modelling or scanning
  + Immersive technologies (virtual reality, augmented reality)
  + Mobile devices (tables, smartphones)
  + None
  + Other (please specify): <sup>[]</sup>


+ Question 9B: Which best describes your students' use of mobile devices? Select all that apply.<sup>9</sup>
  + Referenced information, collected data, or created digital media files using an app
  + Classroom-based, real-time interaction and collaboration (e.g. on documents)
  + Other (please specify): <sup>[]</sup>


+ Question 10: Do your students create and print posters for course projects / assignments?*
  + Yes
  + No


<sup>9</sup> This question was only presented to respondents who, on question 9A, said their students used mobile devices for coursework or class interactions.

##### Section 7: Wrap-Up

+ Question 11: Would you be interested in participating in a technology-related community of practice with other faculty members? Select all that interest you.
  + Learning / Content Management
  + Web Publishing
  + Classroom Technology
  + Data Education
  + Digital Media
  + Emerging Technologies
  + Other (please specify): <sup>[]</sup>


+ Question 12: Is there anything else you'd like us to know about your use of technology in your courses, or do you have questions? <sup>[]</sup>

+ Question 13: May we follow up with you if we have additional questions?*
  + Yes
  + No
